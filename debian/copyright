Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: eblook
Source: http://openlab.jp/edict/eblook/

Files: *
Copyright: 1997-2000, NISHIDA Keisuke
           2000-2002, Satomi
           2000,2001, Kazuhiko
           2000-2002, NEMOTO Takashi
           2000-2001, YAMAGATA
           1997-2000, Motoyuki Kasahara
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: getopt*
Copyright: 1987-1997, Free Software Foundation, Inc.
License: LGPL-2+
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

Files: memcmp.c strdup.c
Copyright: 1988-1993, The Regents of the University of California
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
Comment:
 The advertising clause (3.) remains in the source files, but it was
 removed by the copyright holder.

Files: eblook.texi eblook.info
Copyright: 1997-2000, Keisuke Nishida
           2000-2002, Satomi
           2000-2001, Kazuhiko
           2000-2002, NEMOTO Takashi
License: copyleft-info
 Permission is granted to make and distribute verbatim copies of this
 manual provided the copyright notice and this permission notice are
 preserved on all copies.
 .
 Permission is granted to copy and distribute modified versions of this
 manual under the conditions for verbatim copying, provided that the
 entire resulting derived work is distributed under the terms of a
 permission notice identical to this one.
 .
 Permission is granted to copy and distribute translations of this manual
 into another language, under the above conditions for modified versions,
 except that this permission notice may be stated in a translation
 approved by the Free Software Foundation.

Files: texinfo.tex
Copyright: 1985-2004, Free Software Foundation, Inc.
License: GPL-2+ with Texinfo exception
 This texinfo.tex file is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2, or (at
 your option) any later version.
 .
 This texinfo.tex file is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 As a special exception, when this file is read by TeX when processing
 a Texinfo source document, you may use the result without
 restriction.  (This has been our intent since Texinfo was invented.)
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: config.guess config.sub depcomp ltmain.sh missing
Copyright: 1992-2004, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 As a special exception to the GNU General Public License, if you
 distribute this file as part of a program that contains a
 configuration script generated by Autoconf, you may include it under
 the same distribution terms that you use for the rest of that program.
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: install-sh
Copyright: 1994, X Consortium
License: X11-install-sh
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 .
 FSF changes to this file are in the public domain.

Files: Makefile.in dos/Makefile.in
Copyright: 1994-2004, Free Software Foundation, Inc.
License: permissive-Makefile
 This Makefile.in is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: aclocal.m4
Copyright: 1996-2004, Free Software Foundation, Inc.
License: permissive-aclocal
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: configure
Copyright: 2003, Free Software Foundation, Inc.
License: permissive-configure
 This configure script is free software; the Free Software Foundation
 gives unlimited permission to copy, distribute and modify it.

Files: INSTALL
Copyright: 1994-2002, Free Software Foundation, Inc.
License: permissive-freedoc
 This file is free documentation; the Free Software Foundation gives
 unlimited permission to copy, distribute and modify it.

Files: debian/*
Copyright: 2007-2021, Tatsuya Kinoshita <tats@debian.org>
           1999-2004, Ryuichi Arafune <arafune@debian.org>
License: permissive-debian
 There is no restriction, so this package may be distributed under
 the same conditions as the upstream.
