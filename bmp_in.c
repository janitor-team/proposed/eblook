/* -*- mode: C; c-basic-offset: 4; -*- */

/* bmp_in.c - part of eblook, interactive EB interface command
 *
 *  Decoder for Windows BMP format without compression
 *
 * Copyright (C) 2001 T. Nemoto.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307 USA
 */

/* #define DEBUG */
/* #define BMPTEST */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#ifdef STDC_HEADERS
# include <string.h>
# include <stdlib.h>
#endif

#include "eb/eb.h"
#include "eb/text.h"
#include "eb/font.h"
#include "eb/appendix.h"
#include "eb/error.h"
#include "eb/binary.h"

extern EB_Book current_book;

static unsigned char palette[256][3];

#ifndef BI_BITFIELDS
#define BI_RGB		0
#define BI_RLE8		1
#define BI_RLE4		2
#define BI_BITFIELDS	3
#endif	/* not BI_BITFIELDS */

static ssize_t read_data_from_eb(unsigned char *buf, int len);
static unsigned short get_word(unsigned char *p);
static unsigned long get_dword(unsigned char *p);

static ssize_t read_data_from_eb(buf, len)
     unsigned char *buf;
     int len; 
{
    ssize_t read_length;
    ssize_t count = 0;
    while(len>0) {
	if (eb_read_binary(&current_book, len, (char *)buf, &read_length) 
	    != EB_SUCCESS)
	    return -1;
	if (read_length == 0) 
	    return -1;
	len -= read_length;
	count += read_length;
    }
    return count;
}

#if 0
static unsigned short read_word_from_eb() {
    unsigned char buf[2];
    if (read_data_from_eb(buf,2) == 2) 
	return (((unsigned short)(buf[1]))<<8) + (unsigned short)(buf[0]);
    return (unsigned short)-1;
}
#endif

static unsigned long read_dword_from_eb() {
    unsigned char buf[4];
    if (read_data_from_eb(buf,4) == 4) 
	return (((unsigned long)(buf[3]))<<24) + 
	    (((unsigned long)(buf[2]))<<16) + 
	    (((unsigned long)(buf[1]))<<8) + 
	    (unsigned long)buf[0];
    return (unsigned long)-1;
}

static unsigned short get_word(p)
     unsigned char *p;
{
    unsigned char *buf;
    buf = (unsigned char *) p;
    return (((unsigned short)(buf[1]))<<8) + (unsigned short)(buf[0]);
}

static unsigned long get_dword(p)
     unsigned char *p;
{
    unsigned char *buf;
    buf = (unsigned char *) p;
    return (((unsigned long)(buf[3]))<<24) + 
	(((unsigned long)(buf[2]))<<16) + 
	(((unsigned long)(buf[1]))<<8) + 
	(unsigned long)buf[0];
}

unsigned char *
LoadBMP(w,h) 
     int *w;
     int *h;
{
    unsigned char buf[EB_SIZE_PAGE];
    unsigned char *p;
    unsigned char *bmp, *bmp_pointer, *bmp_end;
    unsigned char *pal;
    unsigned char *output, *output_pointer, *output_end;
    int i;
    long file_size;
    int size;
    int bitcount;
    int compression;
    int bWidth;
    int x,y,x0,x1,y0,y1,yy, output_length;
    
    int n_palette;
    /* unsigned int rgbmask[3]; */

    ssize_t read_length;
    
    int dir = -1;
    int width;

    static unsigned char bit[8]={0x80, 0x40, 0x20, 0x10, 8, 4, 2, 1};
    
    /* BITMAPFILEHEADER size */
    #define bmfh 14

#ifdef BMPTEST
    testfile = fopen("/tmp/test.bmp","r");
#endif

    /* Check BMP magic number */
    if (read_data_from_eb(buf, 2) != 2 || buf[0] != 'B' || buf[1] != 'M')
	return NULL;

    file_size = read_dword_from_eb();
    p = (unsigned char *) malloc(file_size);
    if (p == NULL) 
	return NULL;
    read_length = read_data_from_eb(p+6, file_size-6);
    if (read_length != file_size-6)
	return NULL;
    bmp = p + get_dword(p+10);
    bmp_end = p + file_size;
    size = get_dword(p+bmfh);
    if (size != 40) {
	fprintf(stderr,"OS/2 BMP is not supported yet.\n");
	free(p);
	return NULL;
    }
    n_palette = (get_dword(p+10) - size - bmfh) / 4;
    pal = p + size + bmfh;

#ifdef DEBUG
    fprintf(stderr,"Debug:BMP: n_palette = %d\n",n_palette);
#endif

    width = *w = get_dword(p+18);
    *h = get_dword(p+22);
    if (*h < 0) {
	*h = -*h;
	dir = 1;
    }
#ifdef DEBUG
    fprintf(stderr,"Debug:BMP:w=%d, h=%d, dir=%d\n",*w,*h,dir);
#endif
    output_length = (*w)*(*h)*3;
    output = malloc((*w)*(*h)*3);
    output_end = output + output_length;
    
    if (output == NULL) {
	free(p);
	return NULL;
    }

    for(i=0, output_pointer = output; i< output_length; i++) 
	*(output_pointer++)=0;

    bitcount = get_word(p+28);
    compression = get_dword(p+30);
#ifdef DEBUG
    fprintf(stderr,"Debug:BMP:bitcount=%d, compression=%d\n",
	    bitcount,compression);
#endif

    for(i=0; i<n_palette; i++) {
	palette[i][0] = pal[i*4+2];
	palette[i][1] = pal[i*4+1];
	palette[i][2] = pal[i*4];
    }

    x0=0;
    x1=*w;
    y0=0;
    y1=*h;
    if (dir<0) {
	yy=(*h-1);
    } else {
	yy=0;
    }

    switch(compression) {
    case BI_RGB:
	switch(bitcount) {
	case 1:
	    bWidth = ((width-1)/32+1)*4;
	    for(y=y0;y<y1;y++,yy+=dir){
		for(x=x0;x<x1;x++){
		    for(i=0;i<3;i++){
			if ((bmp[(x>>3)+yy*bWidth] & bit[x & 7]) == 0) {
			    output[(x+y*width)*3+i]=palette[0][i];
			} else {
			    output[(x+y*width)*3+i]=palette[1][i];
			}
		    }
		}
	    }
	    break;
	case 4:
	    bWidth = ((width-1)/8+1)*4;
	    for(y=y0;y<y1;y++,yy+=dir){
		for(x=x0;x<x1;x++){
		    int col;
		    if ((x & 1)==0) {
			col=bmp[(x>>1)+yy*bWidth]>>4;
		    } else {
			col=bmp[(x>>1)+yy*bWidth] & 15;
		    }
		    for(i=0;i<3;i++){
			output[(x+y*width)*3+i]=palette[col][i];
		    }
		}
	    }
	    break;
	case 8:
	    bWidth = ((width-1)/4+1)*4;
	    for(y=y0;y<y1;y++,yy+=dir){
		for(x=x0;x<x1;x++){
		    for(i=0;i<3;i++){
			output[(x+y*width)*3+i]=palette[bmp[x+yy*bWidth]][i];
		    }
		}
	    }
	    break;
	case 16:
#ifdef DEBUG
	    fprintf(stderr,"Debug:BMP: Not Supported! 16bit bitmap\n");
#endif
	    goto failed;
	case 24:
	    bWidth = ((width*3-1)/4+1)*4;
	    for(y=y0;y!=y1;y++,yy+=dir){
		for(x=x0;x<x1;x++){
		    for(i=0;i<3;i++){
			output[(x+y*width)*3+i]=bmp[x*3+yy*bWidth+2-i];
		    }
		}
	    }
	    break;
	case 32:
	    if (n_palette != 3) goto failed;
	    fprintf(stderr,"BMP: Not Supported Yet! 32bit bitmap\n");
	    goto failed;
	default:
	    fprintf(stderr,"BMP: Invalid bitcount\n");
	    goto failed;
	}
	break;
    case BI_RLE8:
	output_pointer = output;
	bmp_pointer = bmp;
	x = x0;
	y = yy;
	while(bmp_pointer < bmp_end && output_pointer < output_end) {
	    int len;
	    len=*(bmp_pointer++);
	    if (len == 0) {
		int esc;
		esc = *(bmp_pointer++);
		switch(esc) {
		case 0:
		    x = x0;
		    y += dir;
		    output_pointer = output + (x+y*width)*3;
		    continue;
		case 1:
		    goto rle8_end;
		case 2:
		    x += *(bmp_pointer++);
		    y += dir * *(bmp_pointer++);
		    output_pointer = output + (x+y*width)*3;
		    continue;
		default:
		    for(i=0;i<esc;i++){
			*(output_pointer++)=palette[*bmp_pointer][0];
			if (output_pointer > output_end) goto failed;
			*(output_pointer++)=palette[*bmp_pointer][1];
			if (output_pointer > output_end) goto failed;
			*(output_pointer++)=palette[*bmp_pointer][2];
			if (output_pointer > output_end) goto failed;
			x++;
			bmp_pointer++;
		    }
		    if (esc & 1) bmp_pointer++;
		}
	    } else {
		for(i=0; i<len; i++) {
		    *(output_pointer++)=palette[*bmp_pointer][0];
		    if (output_pointer > output_end) goto failed;
		    *(output_pointer++)=palette[*bmp_pointer][1];
		    if (output_pointer > output_end) goto failed;
		    *(output_pointer++)=palette[*bmp_pointer][2];
		    if (output_pointer > output_end) goto failed;
		    x++;
		}
		bmp_pointer++;
	    }
	}
    rle8_end:
	break;
    case BI_RLE4:
    case BI_BITFIELDS:
    default:
	fprintf(stderr,"BMP: Unsupported compression code: %d\n",compression);
	goto failed;
    }	
    free(p);
    return output;
 failed:
    free(p);
    free(output);
    return NULL;
}
